// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  firebase: {
    apiKey: "AIzaSyAdi7VnC-gcqnAbGtLJsPtfmTUbeibwLjo",
    authDomain: "crudangularfirebase-563dc.firebaseapp.com",
    projectId: "crudangularfirebase-563dc",
    storageBucket: "crudangularfirebase-563dc.appspot.com",
    messagingSenderId: "682710714572",
    appId: "1:682710714572:web:b97a0b6e962c0f65239a69",
    measurementId: "G-3FJKJTP01X"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
